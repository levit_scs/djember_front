/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'intranet-ng',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },
    ContentSecurityPolicy: {
      'default-src': "'none'",
      'script-src': "'self' 'unsafe-eval'",
      'font-src': "'self'",
      'connect-src': "'self' 'http://127.0.0.1:8000' 'ws://127.0.0.1:8000'",
      'img-src': "'self' 'data:' 'http://www.gravatar.com'",
      'style-src': "'self' 'unsafe-inline'",
      'media-src': "'self'"
    },

    APP: {
      API_NAMESPACE: 'api/v1',
      API_HOST: '',
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };
  
  ENV['simple-auth'] = {
    authorizer: 'authorizer:django',
    authenticationRoute: 'login',
    routeAfterAuthentication: 'index',
    serverTokenEndpoint: '/api/auth/'
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  
    ENV.APP.API_HOST = 'http://devbox.home:8000';
    ENV['simple-auth'].serverTokenEndpoint = ENV.APP.API_HOST + '/api/auth/';
    ENV['simple-auth'].crossOriginWhitelist = [ENV.APP.API_HOST];
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
