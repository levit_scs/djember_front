import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('section', {
  sequences: {
    title: function(num) {
      return 'Section ' + num;
    }
  },
  default: {
    title: FactoryGuy.generate('title'),
    model: null,
    icon: null,
    route: 'index',
    parent: null,
    children: [],
    opts: undefined,
    list_display: ['__str__']
  }

});