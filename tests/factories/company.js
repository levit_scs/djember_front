import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('company', {
  sequences: {
    name: function(num) {
      return 'company_' + num;
    }
  },

  default: {
    name: FactoryGuy.generate('name'),
    is_customer: false,
    is_partner: false,
    is_supplier: false,
    vat: null,
    notes: ''
  }
});