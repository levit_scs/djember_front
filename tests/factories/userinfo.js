import FactoryGuy from 'ember-data-factory-guy';

FactoryGuy.define('userinfo', {
  sequences: {
    username: function(num) {
      return 'user_' + num;
    },
    full_name: function(num) {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < num; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      return text;
    },
    email: function(num) {
      return 'user_' + num + '@example.com';
    }
  },

  default: {
    username: FactoryGuy.generate('username'),
    full_name: FactoryGuy.generate('full_name'),
    email: FactoryGuy.generate('email')
  },

  admin: {
    username: 'admin',
    full_name: 'Admin'
  }
});
