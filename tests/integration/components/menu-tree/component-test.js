import Ember from 'ember';

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

import { make, makeList } from 'ember-data-factory-guy';
import startApp from '../../../helpers/start-app';

var App = null;

moduleForComponent('menu-tree', 'Integration | Component | menu tree', {
  integration: true,
  beforeEach: function () {
    Ember.run(function () {
      App = startApp();
    });
  },
  afterEach: function () {
    Ember.run(App, 'destroy');
  }
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });
  
  var item_no_child = make('section');
  this.set('item', item_no_child);

  this.render(hbs`{{menu-tree item=item}}`);

  assert.equal(this.$().text().trim(), item_no_child.get('title'));


  var children = makeList('section', 2);
  var item_2_children = make('section', {children: children});
  this.set('item', item_2_children);

  this.render(hbs`{{menu-tree item=item}}`);

  assert.equal(this.$().text().trim(), item_2_children.get('title') +
    "\n  \n        " + children[0].get('title') + "\n\n        " +
    children[1].get('title'));

});
