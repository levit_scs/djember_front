import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('djember-ajaxcomplete', 'Integration | Component | djember ajaxcomplete', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(1);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{djember-ajaxcomplete}}`);

  assert.equal(this.$().text().trim(), '');
});
