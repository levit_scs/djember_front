import Ember from 'ember';

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

import { make } from 'ember-data-factory-guy';
import startApp from '../../../helpers/start-app';

var App = null;

moduleForComponent('read-property', 'Integration | Component | read property', {
  integration: true,
  beforeEach: function () {
    Ember.run(function () {
      App = startApp();
    });
  },
  afterEach: function () {
    Ember.run(App, 'destroy');
  }
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  var section = make('section');
  this.set('item', section);

  this.render(hbs`{{read-property model=item property='title'}}`);

  assert.equal(this.$().text().trim(), section.get('title'));

  Ember.run(function () {
    section.set('title', 'other title');
  });

  assert.equal(this.$().text().trim(), 'other title');
});
