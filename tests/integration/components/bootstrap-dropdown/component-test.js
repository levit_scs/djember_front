import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('bootstrap-dropdown', 'Integration | Component | bootstrap dropdown', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{bootstrap-dropdown}}`);

  assert.equal(this.$().text().trim(), 'DropDown');

  // Template block usage:
  this.render(hbs`
    {{#bootstrap-dropdown label=''}}
      template block text
    {{/bootstrap-dropdown}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
