import Ember from 'ember';
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('djember-foreignkey', 'Integration | Component | djember foreignkey', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(1);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  const record = Ember.Object.extend({});

  this.set('content', Ember.A([
    record.create({id: 1, __str__: 'first'}),
    record.create({id: 2, __str__: 'second'})
  ]));

  this.render(hbs`{{djember-foreignkey content=content}}`);

  assert.equal(this.$().text().trim().substring(0,3), '---');
});
