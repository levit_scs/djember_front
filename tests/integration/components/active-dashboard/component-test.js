import Ember from 'ember';

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

import { make /*, makeList */ } from 'ember-data-factory-guy';
import startApp from '../../../helpers/start-app';

var App = null;

moduleForComponent('active-dashboard', 'Integration | Component | active dashboard', {
  integration: true,
  beforeEach: function () {
    Ember.run(function () {
      App = startApp();
    });
  },
  afterEach: function () {
    Ember.run(App, 'destroy');
  }
});

test('it renders', function(assert) {
  assert.expect(1);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });
  
  var item_no_child = make('section');
  this.set('item', item_no_child);

  this.render(hbs`{{active-dashboard item=item}}`);

  assert.equal(this.$().text().trim(), '');

  // Having href-to in the template we can't actually render it
  // during tests, so this test will stay commented out during tests

  // var children = makeList('section', 2);
  // var item_2_children = make('section', {children: children});
  // this.set('testing', true);
  // this.set('item', item_2_children);

  // this.render(hbs`{{active-dashboard}}`);

  // assert.equal(this.$().text().trim(), "\n  \n      " + 
  //   children[0].get('title') + "\n\n      " +
  //   children[1].get('title'));

});
