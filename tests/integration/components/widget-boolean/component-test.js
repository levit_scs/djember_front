import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('widget-boolean', 'Integration | Component | widget boolean', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  var no = {
    value: false
  };

  this.set('record', no);
  this.render(hbs`{{widget-boolean model=record property='value'}}`);

  assert.notEqual(
    this.$('div').html().indexOf('<i class="text-danger fa fa-remove" title="No"'),
    -1
  );


  var yes = {
    value: true
  };

  this.set('record', yes);
  this.render(hbs`{{widget-boolean model=record property='value'}}`);

  assert.notEqual(
    this.$('div').html().indexOf('<i class="text-success fa fa-check" title="Yes"'),
    -1
  );
});
