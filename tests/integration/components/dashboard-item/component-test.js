import Ember from 'ember';

import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

import { make } from 'ember-data-factory-guy';
import startApp from '../../../helpers/start-app';

var App = null;

moduleForComponent('dashboard-item', 'Integration | Component | dashboard item', {
  integration: true,
  beforeEach: function () {
    Ember.run(function () {
      App = startApp();
    });
  },
  afterEach: function () {
    Ember.run(App, 'destroy');
  }
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });
  
  var item_dashboard = make('section', {title: 'Dashboard'});
  this.set('item', item_dashboard);

  this.render(hbs`{{dashboard-item item=item}}`);

  assert.equal(this.$().text().trim(), '');

  var title = 'Not Dashboard';
  var item_not_dashboard = make('section', {title: title});
  this.set('item', item_not_dashboard);


  this.render(hbs`{{dashboard-item item=item}}`);

  assert.equal(this.$().text().trim(), title);
});
