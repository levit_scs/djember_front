import { moduleForModel, test } from 'ember-qunit';

moduleForModel('sellable', 'Unit | Model | sellable', {
  // Specify the other units that are required for this test.
  needs: ['model:section']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
