import { moduleForModel, test } from 'ember-qunit';

moduleForModel('option', 'Unit | Model | option', {
  // Specify the other units that are required for this test.
  needs: ['model:sellable', 'model:section']
});

test('it exists', function(assert) {
  var model = this.subject();
  // var store = this.store();
  assert.ok(!!model);
});
