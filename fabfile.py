import time

from fabric.api import *
from fabric import colors


def git_pull():
    """Does a git stash then a git pull on the project"""
    require('code_root')
    run('cd %s; git stash; git pull' % (env.code_root))


def bower_install():
    """Runs bower install"""
    require('code_root')
    run('cd %s; bower install' % (env.code_root))


def npm_install():
    """Runs pip npm install"""
    require('code_root')
    run('cd %s; npm install' % (env.code_root))


def build():
    """Builds the ember application"""
    require('code_root')
    run('cd %s; ember build' % (env.code_root))


@task
def deploy():
    """Requires code_root and settings_file env variable.
    May also use user and venv_bin_dir env variable.
    Does a full deploy."""
    require('code_root')
    git_pull()
    npm_install()
    bower_install()
    build()


@task
def do_nothing():
    for x in range(0, 20):
        print 'nothing {}'.format(x)
        time.sleep(0.2)

    input = prompt('Enter something:')

    for x in range(0, 20):
        print 'nothing {} - {}'.format(x, input)
        time.sleep(0.2)


@task
def color_test():
    for x in range(0, 2):
        print colors.blue('Blue text', bold=False) + '\n'
        time.sleep(0.2)
        print colors.cyan('cyan text', bold=False)
        time.sleep(0.2)
        print colors.green('green text', bold=False)
        time.sleep(0.2)
        print colors.magenta('magenta text', bold=False)
        time.sleep(0.2)
        print colors.red('red text', bold=False)
        time.sleep(0.2)
        print colors.white('white text', bold=False)
        time.sleep(0.2)
        print colors.yellow('yellow text', bold=False)
        time.sleep(0.2)
        print colors.blue('Blue text bold', bold=True)
        time.sleep(0.2)
        print colors.cyan('cyan text bold', bold=True)
        time.sleep(0.2)
        print colors.green('green text bold', bold=True)
        time.sleep(0.2)
        print colors.magenta('magenta text bold', bold=True)
        time.sleep(0.2)
        print colors.red('red text bold', bold=True)
        time.sleep(0.2)
        print colors.white('white text bold', bold=True)
        time.sleep(0.2)
        print colors.yellow('yellow text bold', bold=True)
        time.sleep(0.2)


@task
def test_env(argument="nothing"):
    print("Task Arguments:")
    print argument
    print

    print("Task Env:")
    for x, y in env.iteritems():
        print '{}: {}'.format(x, y)
