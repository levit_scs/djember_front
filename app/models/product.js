import DS from 'ember-data';
import Sellable from './sellable';

export default Sellable.extend({
  current_stock: DS.attr('number'),
  treshold_stock: DS.attr('number')
});
