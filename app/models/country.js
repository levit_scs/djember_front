import Ember from 'ember';
import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  code2: DS.attr('string'),
  code3: DS.attr('string'),

  __str__: Ember.computed.alias('name')
});
