import Ember from 'ember';
import DS from 'ember-data';
import BaseModel from './base/model';

export default BaseModel.extend({
  name: DS.attr('string'),
  is_customer: DS.attr('boolean'),
  is_partner: DS.attr('boolean'),
  is_supplier: DS.attr('boolean'),
  vat: DS.attr('string'),
  notes: DS.attr('string'),
  expected_income: DS.attr('string'),
  received: DS.attr('string'),
  sector: DS.belongsTo('sector', {
    async: true
  }),
  contactmechanism_set: DS.hasMany('contactmechanism', {
    async: true
  }),
  addresses: DS.hasMany('address', {
    async: true
  }),

  __str__: Ember.computed.alias('name')

});