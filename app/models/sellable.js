import Ember from 'ember';
import DS from 'ember-data';
import BaseModel from './base/model';

export default BaseModel.extend({
  name: DS.attr('string'),
  unit_price: DS.attr('number'),
  type: DS.attr('string'),

  __str__: Ember.computed.alias('name')
});
