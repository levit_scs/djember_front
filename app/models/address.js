import DS from 'ember-data';

export default DS.Model.extend({
  street: DS.attr('string'),
  zip: DS.belongsTo('postalcode', {
    async: true
  }),
  country: DS.belongsTo('country', {
    async: true
  }),
  is_invoice: DS.attr('boolean'),
  is_active: DS.attr('boolean'),

  __str__: function() {
    var street = this.get('street');
    var zip = this.get('zip.__str__');
    return street + ' - ' + zip;
  }.property('street', 'zip')
});
