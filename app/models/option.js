import DS from 'ember-data';
import Sellable from './sellable';

export default Sellable.extend({
  linked_to: DS.belongsTo('sellable', {
    async: true,
    polymorphic: true
  })
});
