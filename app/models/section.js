import Ember from 'ember';

import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  model: DS.attr('string'),
  icon: DS.attr('string'),
  route: DS.attr('string'),
  parent: DS.belongsTo('section', {
    async: true,
    inverse: 'children'
  }),
  children: DS.hasMany('section', {
    async: true,
    inerse: 'parent'
  }),
  opts: DS.attr('raw'),
  list_display: DS.attr('raw'),
  form_display: DS.attr('raw'),
  validations: DS.attr('raw'),

  __str__: Ember.computed.alias('title')
});
