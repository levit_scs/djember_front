import DS from 'ember-data';

export default DS.Model.extend({
  mechanism_type: DS.attr('string'),
  value: DS.attr('string'),
  include_in_mailing: DS.attr('boolean'),
  has_unscubscribed: DS.attr('boolean'),

  __str__: function() {
    return this.get('type') + ': ' + this.get('value');
  }.property('type', 'value')
});
