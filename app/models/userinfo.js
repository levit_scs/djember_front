import DS from 'ember-data';
import BaseModel from './base/model';

export default BaseModel.extend({
  username: DS.attr('string'),
  full_name: DS.attr('string'),
  email: DS.attr('string')
});
