import DS from 'ember-data';

export default DS.Model.extend({
  city: DS.belongsTo('city', {
    async: true
  }),
  code: DS.attr('string'),
  __str__: DS.attr('string')
});
