import Ember from 'ember';
import DS from 'ember-data';
import EmberValidations from 'ember-validations';

export default DS.Model.extend(EmberValidations, {

  init: function() {
    var self = this;
    var section = this.get('store').peekAll('section').find(function(obj) {
      return obj.get('route') === 'list' && obj.get('model') === self.constructor.modelName;
    });
    if (!Ember.isEmpty(section)) {
      this.set('validations', section.get('validations'));
    }
    this._super.apply(this, arguments);
  }

});
