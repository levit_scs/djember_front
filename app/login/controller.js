import Ember from 'ember';

export default Ember.Controller.extend({
  username: null,
  password: null,

  actions: {
    login: function() {
      var credentials = {
        identification: this.get('username'),
        password: this.get('password')
      };
      this.send('authenticate', credentials);
    }
  }
});
