export default [
  {
    title: 'Dashboard',
    route: 'index',
    opts: {
      path: '/'
    }
  }, {
    title: 'CRM',
    route: 'crm',
    icon: 'chat-people.svg',
    sub: [
      {
        title: 'Companies',
        route: 'crm'
      }, {
        title: 'People',
        route: 'crm'
      }
    ]
  }, {
    title: 'Financial',
    route: 'financial',
    icon: 'savings1.svg',
    sub: [
      {
        title: 'Invoices',
        route: 'financial',
        icon: 'bill.svg',
        sub: [
          {
            title: 'Customer invoices',
            route: 'financial'
          }, {
            title: 'Supplier invoices',
            route: 'financial'
          }
        ]
      }, {
        title: 'Reports',
        route: 'financial',
        sub: [
          {
            title: 'VAT',
            route: 'financial'
          }, {
            title: 'Overdue',
            route: 'financial'
          }
        ]
      }
    ]
  }
];
