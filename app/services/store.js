import DS from 'ember-data';
import Ember from 'ember';

export default DS.Store.extend({

  push: function(type, data) {
    var dataType, modelType, oldType; // , oldRecord;
    if(Ember.isArray(type.data) && type.data.length === 0) {
      return this._super(type, data);
    } else {
      modelType = oldType = type.data.type;

      if(type.data.attributes && type.data.attributes.type) {
        dataType = type.data.attributes.type.dasherize(); 
      }

      if(dataType && (this.modelFor(oldType) !== this.modelFor(dataType))) {

        // also ading it to the current store
        this._super(type, data);

        type.data.type = dataType;

        // comment this if you want to add to both stores
        // if(oldRecord = this.peekRecord(oldType, type.data.id)) {
        //   this.unloadRecord(oldRecord);
        // }

      }

      return this._super(type, data);
    }
  }

});