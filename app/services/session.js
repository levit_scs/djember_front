import Ember from 'ember';
import config from '../config/environment';
import SessionService from 'ember-simple-auth/services/session';

export default SessionService.extend({
  currentUser: null,
  loginError: false,
  setCurrentUser: function() {
    if (this.get('isAuthenticated')) {
      var self = this;
      this.authorize('authorizer:django', (headerName, headerValue) => {
        const headers = {};
        headers[headerName] = headerValue;
        Ember.$.ajax(config.APP.API_HOST + '/' + config.APP.API_NAMESPACE + '/me/', {
          dataType: "json",
          headers: headers,
          success: function(json) {
            self.set('currentUser', json);
          }
        });
      });
    } else {
      this.set('currentUser', '');
    }
  }.observes('isAuthenticated')
});
