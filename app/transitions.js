export default function() {
  this.transition(
    this.toRoute('list.detail'),
    this.fromRoute('list.index'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
  this.transition(
    this.fromRoute('list.detail'),
    this.toRoute('list.loading'),
    this.use('toRight')
  );
  this.transition(
    this.use('crossFade', { duration: 150  }),
    this.debug()
  );
}   
