import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('index', {
    path: '/'
  });
  this.route('list', {
    path: '/:model'
  }, function() {
    this.route('detail', {
      path: '/:id'
    });
  });
  this.route('section', {
    path: '/section/:id'
  });
  this.route('login');
  this.route('logout');
});

export default Router;