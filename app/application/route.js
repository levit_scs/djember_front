import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {
  authenticator: 'authenticator:django',
  model() {
    return this.get('store').findAll('section');
  },
  actions: {
    authenticate: function(credentials) {
      var session = this.get('session');
      if (credentials.identification && credentials.password) {
        session.authenticate(this.get('authenticator'), credentials).then(function() {
          session.set('loginError', false);
        }, function() {
          session.set('loginError', "Invalid credentials. Please retry.");
        });
      }
    },
    invalidate: function() {
      this.get('session').invalidate();
    }
  }
});
