import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';
import config from '../config/environment';


function isSecureUrl(url) {
  var link  = document.createElement('a');
  link.href = url;
  link.href = link.href;
  return link.protocol === 'https:';
}


export default Base.extend({

  init: function() {
    var globalConfig = config['simple-auth'] || {};
    this.serverTokenEndpoint = globalConfig.serverTokenEndpoint || '/api-token-auth/';
  },

  authenticate: function(credentials) {
    var self = this;
    return new Ember.RSVP.Promise(function(resolve, reject) {
      var data = { username: credentials.identification, password: credentials.password };
      self.makeRequest(self.serverTokenEndpoint, data).then(function(response) {
        Ember.run(function() {
          resolve(response);
        });
      }, function(xhr /*, status, error */) {
        Ember.run(function() {
          reject(xhr.responseJSON || xhr.responseText);
        });
      });
    });
  },

  restore: function(data) {
    return new Ember.RSVP.Promise(function(resolve, reject) {
      if (!Ember.isEmpty(data.token)) {
        resolve(data);
      } else {
        reject();
      }
    });
  },

  invalidate: function(/* data */) {
    function success(resolve) {
      resolve();
    }
    return new Ember.RSVP.Promise(function(resolve /*, reject */) {
      success(resolve);
    });
  },

  makeRequest: function(url, data) {
    if (!isSecureUrl(url)) {
      Ember.Logger.warn('Credentials are transmitted via an insecure connection - use HTTPS to keep them secure.');
    }
    return Ember.$.ajax({
      url:         url,
      type:        'POST',
      data:        data,
      dataType:    'json',
      contentType: 'application/x-www-form-urlencoded'
    });
  },
});
