import Ember from 'ember';

export default Ember.Controller.extend({
  activeItem: null,

  roots: function() {
    return this.get('model').filter(function(item /*, index, enumerable */) {
      return Ember.isEmpty(item.get('parent.id')) &&
        item.get('slug') !== 'dashboard' &&
        item.get('opts.path') !== '/';
    });
  }.property('model', 'model.@each.parent'),

  actions: {
    activeItem: function(active_item, value) {
      if (value) {
        var wasActive = this.get('activeItem') !== null;
        this.set('activeItem', null);
        this.get('model').forEach(function(item) {
          if (item !== active_item) {
            Ember.set(item, 'active', false);
          }
        });
        if (wasActive) {
          Ember.run.later(this, function() {
            this.set('activeItem', active_item);
          }, 300);
        } else {
          this.set('activeItem', active_item);
        }
      } else {
        this.set('activeItem', null);
      }

    }
  }
});
