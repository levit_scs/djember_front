import Ember from 'ember';
import DjemberInputComponent from '../djember-input/component';

export default DjemberInputComponent.extend({
  type: 'textarea',
  rows: Ember.computed('extra.rows', function() {
    var rows = this.get('extra.rows');
    if (Ember.isEmpty(rows)) {
      return 5;
    }
    return rows;
  })
});
