import Ember from 'ember';

export default Ember.Component.extend({
  model: null,
  property: null,

  editing: Ember.computed.and('editable', 'model.editing'),

  actions: {
    click: function() {
      var editing = this.get('model.editing');
      var model_dirty = this.get('model.hasDirtyAttributes');
      if (!editing || !model_dirty) {
        this.set('model.editing', !editing);
      }
    }
  },
  
  init: function() {
    this._super();
    Ember.Binding.from('model.errors.' + this.get('property')).to('errors').connect(this);
    return Ember.Binding.from("model." + (this.get('property'))).to('value').connect(this);
  }
});
