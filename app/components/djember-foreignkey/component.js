import Ember from 'ember';
import DjemberInputComponent from '../djember-input/component';

export default DjemberInputComponent.extend({
  type: 'select',
  selectedIndex: Ember.computed('value', {
    get(/* key */) {
      var selectedIndex = -1;
      var value = parseInt(this.get('value.id'));
      this.get('content').find(function(item, index) {
        var id = item.get('id');
        if (parseInt(id) === value) {
          selectedIndex = index;
          return true;
        }
        return false;
      });
      return selectedIndex;
    },
    set(key, new_index) {
      this.set('value', this.get('content').objectAt(new_index));
      return new_index;
    }
  })
});
