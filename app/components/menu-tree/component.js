import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  item: null,

  hasChildren: function() {
    var children = this.get('item.children');
    if (!Ember.isEmpty(children)) {
      return true;
    }
    return false;
  }.property('item', 'item.children.length'),

  route_needs_model: function() {
    var route = this.get('item.route');
    return route !== 'index';
  }.property('item', 'item.route')
});
