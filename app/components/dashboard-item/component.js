import Ember from 'ember';

export default Ember.Component.extend({
  item: null,
  activeItemAction: 'activeItem',

  isDashboard: function() {
    var item = this.get('item');
    return item.get('title') === 'Dashboard' || item.get('opts.path') === '/';
  }.property('item', 'item.title'),

  actions: {
    toggle: function() {
      var inverse = !this.get('item.active');
      this.set('item.active', inverse);
      this.sendAction('activeItemAction', this.get('item'), inverse);
    }
  },

  onInit: function() {
    var active = this.get('item.active');
    if (active !== true) {
      this.set('item.active', false);
    }
  }.on('init')
});
