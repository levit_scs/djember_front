import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'ul',
  classNameBindings: ['klass'],
  top: false,
  item: null,

  klass: function() {
    var top = this.get('top');
    if (top) {
      return 'top';
    }
    return null;
  }.property('top')
});
