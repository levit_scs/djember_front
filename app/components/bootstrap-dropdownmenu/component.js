import Ember from 'ember';

export default Ember.Component.extend({
  extraClasses: null,
  classNames: ['dropdown-menu'],
  classNameBindings: ['extraClasses']
});
