import ToManyBase from '../tomany-base/component';

export default ToManyBase.extend({
  currentIndex: 0,
  indexPlusOne: function() {
    var related_length = this.get('related.length');
    if (related_length) {
      var currentIndex = this.get('currentIndex');
      return currentIndex + 1;
    }
    return 0;
  }.property('currentIndex'),
  record: function() {
    var index = this.get('currentIndex');
    var related = this.get('related');
    if (related && index < related.get('length')) {
      return related.objectAt(index);
    }
    return null;
  }.property('related.length', 'currentIndex'),

  direction: 'toLeft',

  actions: {
    previous: function() {
      this.set('direction', 'toRight');
      var index = this.get('currentIndex');
      if (index > 0) {
        this.set('currentIndex', parseInt(index) - 1);
      } else {
        var length = this.get('related.length');
        if (length > 0) {
          this.set('currentIndex', length - 1);
        }
      }
    },
    next: function() {
      this.set('direction', 'toLeft');
      var index = parseInt(this.get('currentIndex'));
      var length = this.get('related.length');
      if (index < length - 1) {
        this.set('currentIndex', index + 1);
      } else if (length > 0) {
        this.set('currentIndex', 0);
      }
    }
  }

});
