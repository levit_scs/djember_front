import Ember from 'ember';

export default Ember.Component.extend({
  layoutName: 'components/djember-input',

  classNames: ['form-group'],
  classNameBindings: ['hasError'],

  label: null,
  type: 'text',
  formLayout: 'horizontal',
  horiClass: 'col-sm-3',
  inputClass: 'col-sm-9',

  isInput: Ember.computed('type', function() {
    var type = this.get('type');
    if (-1 !== ['text', 'phone', 'email', 'number', 'date', 'time', 'datetime'].indexOf(type)) {
      return true;
    }
    return false;
  }),

  hasLabel: Ember.computed('label', 'formLayout', 'type', function() {
    var formLayout = this.get('formLayout');
    var label = this.get('label');
    var type = this.get('type');
    return !Ember.isEmpty(label) && formLayout === 'horizontal' && type !== 'checkbox';
  }),

  bsInputClass: Ember.computed('inputClass', 'hasLabel', function() {
    var hasLabel = this.get('hasLabel');
    if (!hasLabel) {
      return 'col-xs-12';
    }
    return this.get('inputClass');
  }),

  status: Ember.computed('errors.length', function() {
    if (this.get('errors.length')) {
      return 'error';
    } else {
      return 'success';
    }
  }),

  hasError: Ember.computed.equal('status', 'error'),

  bindProperty() {
    Ember.Binding.from('model.' + this.get('property')).to('value').connect(this);
  },

  init() {
    this._super();
    this.bindProperty();
    Ember.Binding.from('model.errors.' + this.get('property')).to('errors').connect(this);
  }

});
