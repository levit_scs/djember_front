import ReadProperty from '../read-property/component';

export default ReadProperty.extend({
  show_status: false,
  has_success: function() {
    return this.get('errors').length === 0 && this.get('show_status');
  }.property('errors', 'show_status'),
  has_error: function() {
    return this.get('errors').length > 0 && this.get('show_status');
  }.property('errors', 'show_status'),

  focusOut: function() {
    this.set('show_status', true);
  },

  onEditingChange: function() {
    var editing = this.get('model.editing');
    if (editing === false) {
      this.set('show_status', false);
    }
  }.observes('model.editing')
});
