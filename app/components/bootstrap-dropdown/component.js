import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['dropdown'],
  extraClasses: null,
  classNameBindings: ['extraClasses'],
  internalTag: 'ul',
  btnClass: 'btn-default',
  label: 'DropDown',
  dropdownClasses: 'from-right'
});
