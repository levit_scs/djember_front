import Ember from 'ember';
import DjemberInputComponent from '../djember-input/component';

export default DjemberInputComponent.extend({
  store: Ember.inject.service(),
  type: 'autocomplete',

  default_min: 3,
  default_debounce: 150,

  recordValue: '',
  valueChanged: Ember.observer('value', function() {
    var self = this;
    this.get('value').then(function(value) {
      if (Ember.isEmpty(value)) {
        self.set('recordValue', '');
      } else {
        self.set('recordValue', value.get('__str__'));
      }
    });
  }),

  linked: Ember.computed.alias('extra.linked'),

  displayValue: Ember.computed('recordValue', 'placeholder', function() {
    var value = this.get('recordValue');
    if (Ember.isEmpty(value)) {
      try {
        return this.get('value.__str__');
      } catch(err) {
        value = this.get('placeholder');
        if (Ember.isEmpty(value)) {
          value = '';
        }
      }
    }
    return value;
  }),

  min_term_length: Ember.computed('extra.min', 'default_min', function() {
    var min = this.get('extra.min');
    if (Ember.isEmpty(min)) {
      return this.get('default_min');
    }
    return min;
  }),
  debounce: Ember.computed('extra.debounce', 'default_debounce', function() {
    var debounce = this.get('extra.debounce');
    if (Ember.isEmpty(debounce)) {
      return this.get('default_debounce');
    }
    return debounce;
  }),

  endpoint: Ember.computed('extra.endpoint', function() {
    var model = this.get('targetModelName');
    if (!Ember.isEmpty(model)) {
      return this.get('store').adapterFor(model).urlForFindAll(model) + '/';
    }
    return '';
  }),

  actions: {
    selectResult(record) {
      this.set('value', this.get('store').findRecord(this.get('targetModelName'), record.id));
      this.valueChanged();
    }
  }
});
