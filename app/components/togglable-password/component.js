import Ember from 'ember';

export default Ember.Component.extend({
  classNames: 'input-group',
  type: 'password',

  value: null,

  visible: function() {
    return this.get('type') !== 'password';
  }.property('type'),

  actions: {
    toggle: function() {
      this.set('type', this.get('type') === 'password' ? 'text' : 'password');
    }
  }
});
