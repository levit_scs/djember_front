import Ember from 'ember';
import HyperSearchComponent from 'ember-hypersearch/components/hyper-search';

const {
  RSVP: { Promise, resolve },
  $: { ajax },
  get,
  set
} = Ember;

function keyForQuery(query) {
  return `_cache.${query}`;
}

export default HyperSearchComponent.extend({
  resultClass: 'hidden',
  loadIndicatorClass: 'hidden',
  doSearch: false,
  reset: null,

  cache(query, results) {
    if (results.hasOwnProperty('results')) {
      results = results.results;
    }
    set(this, keyForQuery(query), results);
    return resolve(results);
  },

  _handleAction(actionName, ...args) {
    this.set('loadIndicatorClass', 'hidden');
    if (actionName === 'selectResult') {
      this.set('resultClass', 'hidden');
      this.set('doSearch', false);
      var reset = this.get('reset');
      if (!Ember.isEmpty(reset)) {
        Ember.run.cancel(reset);
        this.set('reset', null);
      }
    } else {
      this.set('resultClass', '');
    }
    this._super(actionName, ...args);
  },

  get_data() {
    var linked = this.get('linked');
    var rv = {};
    var self = this;
    if (!Ember.isEmpty(linked)) {
      linked.forEach(function(linked_item) {
        var value = self.get('model.' + linked_item.property);
        if (!Ember.isEmpty(value)) {
          rv[linked_item.filter] = value;
        }
      });
    }
    return rv;
  },

  request(query) {
    var data = this.get_data();
    data.q = query;
    return new Promise((resolve, reject) => {
      ajax({
        dataType: 'json',
        method: 'GET',
        url: get(this, 'endpoint'),
        data: data
      })
      .then(resolve, reject);
    });
  },

  didInsertElement() {
    this._super();
    var self = this;
    Ember.run.scheduleOnce('afterRender', this, function() {
      self.$('.hypersearch-input').on('focusout', function() {
        self.set('reset', Ember.run.debounce(this, function() {
          if (self.$().find(':focus').length === 0) {
            self.resetInput();
          }
        }, 150));
      });
    });
  }, 

  resetInput() {
    this.set('resultClass', 'hidden');
    var value = this.get('value');
    this.set('doSearch', false);
    this.$('.hypersearch-input').val(value);
  },

  actions: {
    search(_event, query) {
      var search = this.get('doSearch');
      if (search) {
        this.set('loadIndicatorClass', '');
        this._super(_event, query);
      } else {
        this.set('doSearch', true);
      }
    }
  }
});
