import Ember from 'ember';

export default Ember.Component.extend({
  tagName: null,
  horiClass: 'col-sm-3',
  inputClass: 'col-sm-9',

  isFieldset: Ember.computed.equal('field.widget', 'fieldset'),
  isTabset: Ember.computed.equal('field.widget', 'tabset'),

  isToManyTable: Ember.computed.equal('field.widget', 'tomany-table'),
  isToManySlide: Ember.computed.equal('field.widget', 'tomany-slide'),

  isToMany: Ember.computed.or('isToManyTable', 'isToManySlide'),

  isCollection: Ember.computed.or('isFieldset', 'isTabset', 'isToMany'),

  collectionComponent: function() {
    var sets = [
      ['isFieldset', 'form-fieldset'],
      ['isTabset', 'form-tabset'],
    ];
    var set_val, i, l, set;
    for (i=0, l=sets.length; i<l; i++) {
      set = sets[i];
      set_val = this.get(set[0]);
      if (set_val === true) {
        return set[1];
      }
    }
    return this.get('field.widget');
  }.property('isFieldset', 'isTabset', 'isToManyTable'),

  field_horiClass: function() {
    var cls = this.get('field.horiClass');
    if (cls) {
      return cls;
    }
    return this.get('horiClass');
  }.property('field.horiClass'),
  field_inputClass: function() {
    var cls = this.get('field.inputClass');
    if (cls) {
      return cls;
    }
    return this.get('inputClass');
  }.property('field.inputClass')
});
