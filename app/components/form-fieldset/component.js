import Ember from 'ember';

export default Ember.Component.extend({
  tagName: null,
  innerClass: function() {
    var innerClass = this.get('field.innerClass');
    if (!Ember.isEmpty(innerClass)) {
      return innerClass;
    }
    return 'col-xs-12';
  }.property('field.innerClass'),
  fields: function() {
    var rv = Ember.A();
    var self = this;
    var innerHoriClass = this.get('field.innerHoriClass');
    if (Ember.isEmpty(innerHoriClass)) {
      innerHoriClass = 'col-sm-3';
    }
    var innerInputClass = this.get('field.innerInputClass');
    if (Ember.isEmpty(innerInputClass)) {
      innerInputClass = 'col-sm-9';
    }
    var fields = this.get('field.fields');
    if (!Ember.isEmpty(fields)) {
      fields.forEach(function(field) {
        if (Ember.isEmpty(field.horiClass)) {
          field.horiClass = innerHoriClass;
        }
        if (Ember.isEmpty(field.inputClass)) {
          field.inputClass = innerInputClass;
        }

        if (!Ember.isEmpty(field.model) && field.targetModel === undefined) {
          console.log('fetching data for model ' + field.model);
          console.log(field.targetModel);
          var target_model = self.get('store').findAll(field.model);
          field.targetModel = target_model;
        }
        rv.pushObject(field);
      });
    }
    return rv;
  }.property('field.fields', 'field.fields.length')
});
