import Ember from 'ember';
import config from '../../config/environment';

export default Ember.Component.extend({
  tagName: 'nav',
  classNames: ['sideNav', 'navmenu', 'navmenu-inverse', 'navmenu-fixed-left', 'offcanvas'],
  ariaRole: 'navigation',
  sections: [],
  api_host: function() {
    return config.APP.API_HOST;
  }.property(),
  api_namespace: function() {
    return config.APP.API_NAMESPACE;
  }.property(),

  didInsertElement: function() {
    var self = this;
    Ember.run.scheduleOnce('afterRender', this, function() {
      self.$().on('click', 'a', function() {
        self.$().offcanvas('hide');
      });
    });
  },

  roots: function() {
    var sections = this.get('sections');
    return sections.filter(function(item /*, index, enumerable */) {
      var parent = item.get('parent.id');
      return Ember.isEmpty(parent);
    });
  }.property('sections', 'sections.@each.parent')
});
