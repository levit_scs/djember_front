import Ember from 'ember';

export default Ember.Component.extend({
  related: Ember.computed.alias('value'),

  init: function() {
    this._super();
    return Ember.Binding.from("model." + (this.get('field.field'))).to('value').connect(this);
  }
});
