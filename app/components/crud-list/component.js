import Ember from 'ember';

export default Ember.Component.extend({

  data: Ember.computed.alias('model'),
  newActionName: 'new',

/*  onModelChange: function() {
    console.log('modelChange');
    this.rerender();
  }.observes('model'),
*/
  actions: {

    new: function() {
      this.sendAction('newActionName');
    },

    save: function(record) {
      if (record.editing && record.isValid && record.hasDirtyAttributes) {
        record.set('saving', true);
        record.save().then(function() {
          record.set('editing', false);
          record.set('saving', false);
        });
      }
    },

    cancel: function(record) {
      if (record.editing) {
        if (record.hasDirtyAttributes) {
          record.rollback();
        }
        record.set('editing', false);
      }
    }
  }
});
