import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {

  meta_model: null,

  model: function() {
    var model = this.modelFor('list');
    if (Ember.isEmpty(model.type)) {
      // We received a section
      var meta_model =  model.get('model');
      this.set('meta_model', meta_model);
      return this.get('store').findAll(meta_model);
    } else {
      // We received the actual list of models
      this.set('meta_model', model.type.modelName);
      return model;
    }
  },

  setupController: function(controller, model) {
    var self = this;
    controller.set('model', model);
    controller.set('meta_model', this.get('store').peekAll('section').find(function(item /*, index, enumerable */) {
      return item.get('route') === 'list' && item.get('model') === self.get('meta_model');
    }));
  }

});
