import Ember from 'ember';

export default Ember.Controller.extend({

  actions: {

    new: function() {
      var record = this.get('store').createRecord(this.get('meta_model.model'), {});
      record.set('editing', true);
    }
  }
 });
