import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  meta_model: null,
  model: function(params) {
    this.set('meta_model', params.model);
    var rv = this.store.findAll(params.model);
    return rv;
  },

  setupController: function(controller, model) {
    var self = this;
    controller.set('model', model);
    controller.set('meta_model', this.get('store').peekAll('section').find(function(item /*, index, enumerable */) {
      return item.get('route') === 'list' && item.get('model') === self.get('meta_model');
    }));
  },

  serialize: function(model) {
    return { model: model.get('model') };
  }
});
