import Ember from 'ember';
import Base from 'ember-simple-auth/authorizers/base';

export default Base.extend({
  session: Ember.inject.service(),
  authorize: function(requestData, block) {
    var secureData = this.get('session.data.secure');
    var accessToken = secureData['token'];
    if (this.get('session.isAuthenticated') && !Ember.isEmpty(accessToken)) {
      block('Authorization', 'Token ' + accessToken);
    }
  }
});
