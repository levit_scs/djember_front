/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    svg: {
      paths: [
        'public/assets/svgs'
      ]
    }    
  });
  
  app.import('vendor/bootstrap/bootstrap.js');
  app.import('vendor/jasny-bootstrap.js');

  return app.toTree();
};
